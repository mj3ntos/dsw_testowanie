Feature: Login
  Scenario Outline: Login User
    Given I am on the login page
    When User pass login data email:<email> password:<password>
    Then User is logged in the system
    And Displays the token

    Examples:
    | email              |  password  |
    | eve.holt@reqres.in | cityslicka |