from behave import *
import requests
import json

base_url = "https://reqres.in/api/"

headers = {
        'Content-Type': 'application/json'
    }

@step('I am in app')
def step_impl(context):
    pass

@step('User pass data email:{email} password:{password}')
def step_impl(context, email, password):
    payload = json.dumps({
        "email": email,
        "password": password
    })

    URL = base_url + "register"
    response = requests.request("POST", URL, headers=headers, data=payload)
    context.response = response
    context.response_data = response.json()


@step('User is registered in the system')
def step_impl(context):
    assert context.response.status_code == 200
    assert 'id' in context.response_data
    assert 'token' in context.response_data
    context.id = context.response_data['id']
    context.token = context.response_data['token']


@step('Displays id and token')
def step_impl(context):
    print(f"id: {context.id}")
    print(f"Token: {context.token}")