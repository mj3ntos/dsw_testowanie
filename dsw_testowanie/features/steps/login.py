from behave import *
import requests
import json

base_url = "https://reqres.in/api/"

headers = {
  'Content-Type': 'application/json'
}


@step('I am on the login page')
def step_impl(context):
    pass


@step('User pass login data email:{email} password:{password}')
def step_impl(context, email, password):
    payload = json.dumps({
        "email": email,
        "password": password
    })
    URL = base_url + "login"
    response = requests.request("POST", URL, data=payload, headers=headers)
    context.response = response
    context.response_data = response.json()


@step('User is logged in the system')
def step_impl(context):
    assert context.response.status_code == 200
    assert 'token' in context.response_data
    context.token = context.response_data['token']


@step('Displays the token')
def step_impl(context):
    print(f"Token: {context.token}")