Feature: Register
  Scenario Outline: Register User
    Given I am in app
    When User pass data email:<email> password:<password>
    Then User is registered in the system
    And Displays id and token
    Examples:
      |        email         | password |
      | eve.holt@reqres.in   |  pistol  |