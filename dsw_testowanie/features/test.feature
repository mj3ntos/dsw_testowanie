Feature: Users

  Scenario Outline: Create User
    Given I am  in cool app
    When User is created with name:<name> job:<job>
    Then Created user exist in the system
    And User name is <name>
    And User job is <job>

    Examples:
      | name | job |
      | pif  | paf |
      | 123  | 321 |